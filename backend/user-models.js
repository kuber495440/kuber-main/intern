import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
	fullName : {
		type : String,
		required : [true, "Full Name is required"]
	},
	email : {
		type : String,
		required : [true, "email is required"]
	},
	password : {
		type : String,
		required : [true, "password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	}
})