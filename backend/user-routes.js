import express from 'express';
import { getUsers, getUser, createUser, loginUser } from './user-controllers.js'
import bcrypt from 'bcrypt'
// const router = express.Router();

const app = express();

app.use(express.json());

app.get("/users", async (req, res) => {
	const users = await getUsers();
	res.send(users);
})

app.get("/user/:id", async (req, res) => {
	const id = req.params.id;
	const user = await getUser(id);
	res.status(201).send(user);
})

app.post("/register", async (req, res) => {
	const { email, password } = req.body;
	const result = await createUser(email, password);
	res.send(result);
})

app.post("/login", async (req, res) => {
	const { email, password } = req.body;
	const result = await loginUser(email, password);
	res.send(result);
})

app.use((err, req, res, next) => {
	console.error(err.stack)
	res.status(500).send('Something broke!')
})

app.listen(8080, () => {
	console.log('Server is running on port 8080');
})