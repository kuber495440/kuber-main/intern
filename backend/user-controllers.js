// const mysql = require('mysql2');
import mysql from 'mysql2';

// const dotenv = require('dotenv');
import dotenv from 'dotenv';
dotenv.config();

const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE
}).promise();


export async function getUsers() {
  const [rows] = await pool.query("SELECT * FROM users");
  return rows;  
}

export async function getUser(id){
  const [rows] = await pool.query(`SELECT * FROM users WHERE id = ?`, [id])
  return rows;
}

export async function createUser(email, password){
  const [result] = await pool.query(`
    INSERT INTO users (email, password)
    VALUES (?, ?)
    `, [email, password])
  const id = result.insertId;
  return getUser(id);
}

export async function loginUser(email, password){
  try{
    const [existingUser] = await pool.query(
      'SELECT * FROM users WHERE email = ?', 
      [email]
    );

    if(!existingUser.length){
      return { error: 'User not found' };
    }
    else{
      if(existingUser[0].password !== password){
        return { error: 'Incorrect password' };
      }
      else{
        return { user: existingUser[0] };
      }
    }
  }catch(error){
    return{ error: 'An error occured' }
  }
}

// const result = await createUser('light', 'yagami');
// console.log(result);

